<table class="table table-bordered">
    <tr>
        <td>
            Categorie
        </td>
        <td>
            Produit
        </td>
        <td>
            Fournisseur
        </td>
        <td>
            Prix d'achat
        </td>
        <td>
            Quantite
        </td>
        <td>
            Type de qte
        </td> 
        <td>
            Prix de vente
        </td>            
        <td>
            Magasin
        </td>           
    </tr>
<?php
// Sous WAMP (Windows)
    try {
        $bdd = new PDO('mysql:host=localhost;dbname=wholesaler;charset=utf8', 'root', '');
    }
    catch (Exception $e){
            die('Erreur : ' . $e->getMessage());
    }

    $reponse = $bdd->query('SELECT * FROM products');
    
    while($donnees = $reponse->fetch()){ ?>

    <tr>
        <td>
            <?php echo $donnees['Cathegory']; ?> 
        </td>
        <td>
            <?php echo $donnees['Product']; ?> 
        </td>
        <td>
            <?php echo $donnees['Supplier']; ?> 
        </td>
        <td>
            <?php echo $donnees['PriceOfPurchase']; ?> 
        </td>
        <td>
            <?php echo $donnees['Quantity']; ?> 
        </td>
        <td>
            <?php echo $donnees['TypeOfQuantity']; ?> 
        </td> 
        <td>
            <?php echo $donnees['PriceOfSale']; ?> 
        </td>            
        <td>
            <?php echo $donnees['Shop']; ?> 
        </td>           
    </tr>

    <?php }

?>                
</table>
