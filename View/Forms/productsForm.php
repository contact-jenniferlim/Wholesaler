<div class="row">
    <h3> Chez le grossiste </h3>
</div>

<form class="form-inline" action="Controller/addproduct.php" method="post">
    <div class="form-group">
        <select class="form-control" name="Cathegory">
            <option value="Categorie">Categorie</option>
            <option value="Fruits">Fruits</option>
            <option value="Legumes">Legumes</option>
        </select>
    </div>
     <div class="form-group">
        <input type="text" name="Product" class="form-control" placeholder="Produit">        
    </div>
    <div class="form-group">
        <input type="text" name="Supplier" class="form-control" placeholder="Fournisseur">        
    </div>
    <div class="form-group">
        <input type="text" name="PriceOfPurchase" class="form-control" placeholder="Prix d'achat">        
    </div>  
    <div class="form-group">
        <input type="text" name="Quantity" class="form-control" placeholder="Quantite">        
    </div>
    <div class="form-group">
        <select class="form-control" name="TypeOfQuantity">
            <option value="Unite">Unite</option>
            <option value="Barquette">Barquette</option>
        </select>        
    </div>    
    <div class="form-group">
        <input type="text" name="PriceOfSale" class="form-control" placeholder="Prix de vente">        
    </div>
    <div class="form-group">
        <input type="text" name="Shop" class="form-control" placeholder="Magasin">        
    </div>               
  <button type="submit" class="btn btn-default">Ajouter</button>
</form>