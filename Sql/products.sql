-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 24 Avril 2015 à 16:18
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `wholesaler`
--

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `Cathegory` text NOT NULL,
  `Product` text NOT NULL,
  `Supplier` text NOT NULL,
  `PriceOfPurchase` float(10,2) NOT NULL,
  `Quantity` text NOT NULL,
  `TypeOfQuantity` text NOT NULL,
  `PriceOfSale` float(10,2) NOT NULL,
  `Shop` text NOT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Contenu de la table `products`
--

INSERT INTO `products` (`Cathegory`, `Product`, `Supplier`, `PriceOfPurchase`, `Quantity`, `TypeOfQuantity`, `PriceOfSale`, `Shop`, `Id`) VALUES
('Fruits', 'Fraise', 'Primeur', 0.99, '100', 'Barquette', 1.50, 'Carrefour', 1),
('Legumes', 'Courgette', 'Primeur', 0.90, '50', 'Unite', 1.10, 'Carrefour', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
